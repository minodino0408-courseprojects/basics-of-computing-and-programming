bmiStatus = ["Underweight", "Normal", "Overweight", "Obese"]

reportedWeight = float(input('Please enter weight in kilograms:'))
reportedHeight = float(input('Please enter height in meters:'))

bmi = round(reportedWeight / (reportedHeight ** 2), 2)

if (bmi < 18.5):
    print("BMI is: " + str(bmi) + ", Status is " + bmiStatus[0]);
elif (bmi > 18.4 and bmi < 24.5):
    print("BMI is: " + str(bmi) + ", Status is " + bmiStatus[1])
elif (bmi >= 25.0 and bmi <= 29.9):
    print("BMI is: " + str(bmi) + ", Status is " + bmiStatus[2])
else:
    print("BMI is: " + str(bmi) + ", Status is " + bmiStatus[3])