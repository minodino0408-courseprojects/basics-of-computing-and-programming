'''Description

Write a function that is given a phrase and returns the first word in that phrase.
For example, given ‘the quick brown fox’, your function should return ‘the’.

Here is an example call to the function

print(firstword(“the quick brown fox”)):
'''
#def main():
#    s1 = str(input("Enter a sentence: "))
#    print(firstword(s1))

def firstword(s1):
    word = s1.split(" ")
    return word[0]

#main()