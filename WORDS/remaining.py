'''
Description

Write a function that is given a phrase and returns the phrase we get if we take  
out the first word from the input phrase.
For example, given ‘the quick brown fox’, your function should return ‘quick brown fox’


Here is an example call to the function

print(remainingwords(“the quick brown fox”)):
'''
def remainingwords(s1):
    word = s1.split()
    if len(word) <= 1:
        return ""
    else: 
        wordslist = word[1:len(word)]
        remainingwords = " "
        remainingwords = remainingwords.join(wordslist)
    return remainingwords