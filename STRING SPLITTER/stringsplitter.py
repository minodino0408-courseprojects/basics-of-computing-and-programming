"""
Description 
Read from the user a string containing odd number of characters. Your program should: 
a) Print the middle character. 
b) Print the string up to but not including the middle character (i.e., the first half 
of the string). 
c) Print the string from the middle character to the end (not including the 
middle character). 

Sample output: 
Enter an odd length string: Fortune favors the bold
Middle character: o
First half: Fortune fav
Second half: rs the bold
"""

s1_input = str(input("Enter an odd length string: "))

s1_length = len(s1_input)

mid_char_index = int(s1_length / 2)
mid_char = s1_input[mid_char_index]

first_half = s1_input[0:mid_char_index]

second_half = s1_input[(mid_char_index +1) : s1_length]

print("Middle character: ", mid_char)
print("First half: ", first_half)
print("Second half: ", second_half)