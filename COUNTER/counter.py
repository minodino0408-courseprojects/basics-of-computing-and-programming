##Getting user input for values of coins
print("Please enter the number of coins:") 
##\n# of quarters")
numOfQuarters = int(input())
##print("# of dimes:")
numOfDimes = int(input())
##print("# of nickles:")
numOfNickles = int(input())
##print("# of pennies:")
numOfPennies = int(input())

##Quarter data
dollarInQuarters = numOfQuarters // 4
remainingQuarters = (numOfQuarters % 4) * 25

##Dime data
dollarInDimes = numOfDimes // 10
remainingDimes = (numOfDimes % 10) * 10

##Nickle data
dollarInNickles = numOfNickles // 20
remainingNickles = (numOfNickles % 20) * 5

##Penny data
dollarInPennies = numOfPennies // 100 
remainingPennines = numOfPennies % 100

##Adding all values & Converting change into dollars and finding the actual remaining amount
totalRemainingChange = remainingQuarters + remainingDimes + remainingNickles + remainingPennines
changeIntoDollars = totalRemainingChange // 100
totalDollars = dollarInQuarters + dollarInDimes + dollarInNickles + dollarInPennies + changeIntoDollars
changeOfChange =  totalRemainingChange % 100 

print("The total is", totalDollars, "dollars and", changeOfChange, "cents")