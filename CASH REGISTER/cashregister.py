firstItem = float(input())#"Enter the price of the first item: "#))
secondItem = float(input())#"Enter the price of the second item: "))
clubCardMember = str(input())#"Does the customer hace a club card? (Y/N): "))

memberDiscount = 0
if clubCardMember == "y" or clubCardMember == "Y":
    memberDiscount = 10/100
elif clubCardMember == "n" or clubCardMember == "N":
    memberDiscount == 0/100

bogoPromo = 0
if secondItem <= firstItem:
    bogoPromo = (secondItem / 2)
elif firstItem <= secondItem:
    bogoPromo = (firstItem / 2)
else: 
    bogoPromo = 0

taxRate = float(input())#"Enter tax rate:"))

basePrice = firstItem + secondItem
print("Base Price =% 2.2f" %basePrice)

finalPrice = basePrice - bogoPromo

discountedPrice = finalPrice - (memberDiscount * finalPrice)
print("Price after discounts =% 2.2f" %discountedPrice)

salesTax = (taxRate/100)*discountedPrice

totalCost = discountedPrice + salesTax
print("Total price =", round(totalCost,2))