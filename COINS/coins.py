from decimal import Decimal

##Set data values
coinNames = ["quarters", "dimes", "nickels", "pennies"]
coinValues = {'quarters':0.25, 'dimes':0.10, 'nickels':0.05, 'pennies':0.01}

##User input
print("Please enter the amount of money to convert:")
dollars = int(input())
dollarAmount = float(dollars * 1.00)
cents = int(input())
centAmount = float(cents * 0.01)
totalAmount = dollarAmount + centAmount

##Calculations
minusQuarters = int(totalAmount // coinValues.get('quarters'))
remaining1 = totalAmount % coinValues.get('quarters')
minusDimes = int(remaining1 // coinValues.get('dimes'))
remaining2 = remaining1 % coinValues.get('dimes')
minusNickels = int(remaining2 // coinValues.get('nickels'))
remaining_3 =  remaining2 % coinValues.get('nickels')
remaining3 = round(remaining_3, 2)
minusPennies = int(remaining3 // coinValues.get('pennies'))

##Convert to strings
totalQuarters = str(minusQuarters)
totalDimes = str(minusDimes)
totalNickels = str(minusNickels)
totalPennies = str(minusPennies)

print("The coins are "+ totalQuarters + " " + coinNames[0] + ", " + totalDimes + " " + coinNames[1] + ", " + totalNickels + " " + coinNames[2] + " and " + totalPennies + " " + coinNames[3])