day = str(input("Enter the day the call started at: "))

time = int(input("Enter the time the call started at (hhmm): "))

minutes = float(input("Enter the duration of the call (in minutes): "))

billedRate = 0.00
if day == "Sat" or day == "Sun":
    billedRate = 0.15 * minutes
else:
    if (time >= 800) and (time <= 1800):
        billedRate = 0.40 * minutes
    elif(time < 800) or (time > 1800):
        billedRate = 0.25 * minutes

print("This call will cost $%2.2f" %billedRate)