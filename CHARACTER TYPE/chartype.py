'''
Description

Write a program that  reads a character (string of  length  1)  from  the 
user, and classifies it to  one of the following: lower case letter, upper case letter, 
digit,  or non-alphanumeric character 

Sample  output (4 different executions):  
Enter a character: j
j is  a lower case  letter.
Enter a character: 7
7 is  a digit.
Enter a character: ^
^ is  a non-alphanumeric  character.
Enter a character: C
C is  an  upper case  letter.
'''

c1_input = input("Enter a character: ")

c2 = c1_input.isdigit()
c3 = c1_input.islower()
c4 = c1_input.isupper()

if c2 == True:
    print(c1_input, "is a digit.")
elif c3 == True:
    print(c1_input, "is a lower case letter.")
elif c4 == True:
    print(c1_input, "is an upper case letter.")
else:
    print(c1_input, "is a non-alphanumeric character.")